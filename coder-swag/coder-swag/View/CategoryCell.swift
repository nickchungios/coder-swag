//
//  CategoryCell.swift
//  coder-swag
//
//  Created by cjnora on 2017/12/19.
//  Copyright © 2017年 nickchunglolz. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!

    func updateView(category: Category){
        categoryImage.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    }

}
